<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Services;

use DateTimeInterface;
use Inisiatif\Component\Auth\Models\User;
use Inisiatif\Component\Contract\User\Model\UserInterface;
use Inisiatif\Component\Resource\Laravel\Eloquent\Service;
use Inisiatif\Component\Contract\User\Service\UserServiceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
final class UserService extends Service implements UserServiceInterface
{
    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function save(UserInterface $user): UserInterface
    {
        $this->beforeAction($user);

        /** @var User $model */
        $model = clone $user;

        if ($model->exists) {
            $model->update();

            return $model;
        }

        $model->save();

        return $model;
    }

    /**
     * @param UserInterface $user
     * @param string        $newRawPassword
     *
     * @return bool
     */
    public function changePassword(UserInterface $user, string $newRawPassword): bool
    {
        $user->setPassword(\bcrypt($newRawPassword));

        $this->save($user);

        return true;
    }

    /**
     * @param UserInterface     $user
     * @param DateTimeInterface $date
     *
     * @return bool
     */
    public function updateLastLogin(UserInterface $user, DateTimeInterface $date): bool
    {
        $this->beforeAction($user);

        /** @var User $model */
        $model = clone $user;

        $model->setLastLogin($date);

        return $model->save();
    }
}
