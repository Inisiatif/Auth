<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth;

use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Inisiatif\Component\Auth\Models\User;
use Spatie\Permission\PermissionServiceProvider;
use Inisiatif\Component\Auth\Repositories\UserRepository;
use Inisiatif\Component\Auth\Providers\EventServiceProvider;
use Inisiatif\Component\Auth\Security\InisiatifUserProvider;
use Illuminate\Contracts\Container\BindingResolutionException;
use Inisiatif\Component\Resource\Laravel\Support\ServiceProvider;
use SocialiteProviders\Manager\ServiceProvider as SocialiteServiceProvider;
use Inisiatif\Component\Resource\Laravel\Traits\HasAutoBindingServiceTrait;
use Tymon\JWTAuth\Providers\LaravelServiceProvider as JWTAuthServiceProvider;
use Inisiatif\Component\Contract\Resource\Model\HasLegacyIntranetIdentifierInterface;
use Inisiatif\Component\Contract\Resource\Repository\HasLegacyIntranetRepositoryInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
final class InisiatifAuthServiceProvider extends ServiceProvider
{
    use HasAutoBindingServiceTrait;

    /**
     * @throws BindingResolutionException
     */
    public function register(): void
    {
        $this->registerAssetResource(__DIR__, 'auth');

        $this->registerServiceContainer();

        $this->registerServiceProvider();

        $this->registerAuthUserProvider();
    }

    protected function registerAuthUserProvider(): void
    {
        Auth::provider('inisiatif', function ($app, array $config): InisiatifUserProvider {
            return new InisiatifUserProvider($app['hash'], $config['model']);
        });
    }

    protected function registerServiceProvider(): void
    {
        $this->app->register(JWTAuthServiceProvider::class);
        $this->app->register(EventServiceProvider::class);
        $this->app->register(SocialiteServiceProvider::class);
        $this->app->register(PermissionServiceProvider::class);
    }

    protected function registerServiceContainer(): void
    {
        $this->bindInterfaceFromDirectory(__DIR__ . DIRECTORY_SEPARATOR . 'Repositories');
        $this->bindInterfaceFromDirectory(__DIR__ . DIRECTORY_SEPARATOR . 'Models');
        $this->bindInterfaceFromDirectory(__DIR__ . DIRECTORY_SEPARATOR . 'Services');

        $this->app->bind(JWTSubject::class, User::class);
        $this->app->bind(HasLegacyIntranetIdentifierInterface::class, User::class);
        $this->app->bind(HasLegacyIntranetRepositoryInterface::class, UserRepository::class);
    }

    /**
     * @return string
     */
    protected function getNamespaceComponent(): string
    {
        return __NAMESPACE__;
    }
}
