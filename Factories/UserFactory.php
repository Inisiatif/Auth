<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Factories;

use Illuminate\Support\Str;
use Laravel\Socialite\AbstractUser;
use Inisiatif\Component\Auth\Models\User;
use Inisiatif\Component\Contract\User\Model\UserInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
final class UserFactory
{
    /**
     * @param AbstractUser $user
     *
     * @return UserInterface
     */
    public static function buildFromOAuth(AbstractUser $user): UserInterface
    {
        $rawPassword = Str::random(6);

        $model = new User();
        $model->setName($user->getName());
        $model->setEmail($user->getEmail());
        $model->setIntranetId($user->getId());
        $model->setToggle(true);

        $model->setPassword(\bcrypt($rawPassword));

        return $model;
    }
}
