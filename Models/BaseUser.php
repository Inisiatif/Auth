<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Models;

use Inisiatif\Component\Resource\Laravel\Model\User;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
abstract class BaseUser extends User
{
}
