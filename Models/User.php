<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Models;

use Inisiatif\Component\Contract\Resource\Model\ToggleAwareInterface;
use Inisiatif\Component\Contract\Resource\Model\HasLegacyIntranetIdentifierInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
final class User extends BaseUser implements HasLegacyIntranetIdentifierInterface, ToggleAwareInterface
{
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'last_login',
        'change_password_at', 'intranet_id', 'is_active',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'intranet_id' => 'int',
        'is_active' => 'bool',
    ];

    /**
     * @return string|integer
     */
    public function getIntranetId()
    {
        return $this->getAttribute('intranet_id');
    }

    /**
     * @param string|int|null $value
     *
     * @return HasLegacyIntranetIdentifierInterface|self
     */
    public function setIntranetId($value): HasLegacyIntranetIdentifierInterface
    {
        return $this->setAttribute('intranet_id', $value);
    }

    /**
     * @param bool $value
     *
     * @return ToggleAwareInterface|self
     */
    public function setToggle(bool $value): ToggleAwareInterface
    {
        return $this->setAttribute('is_active', $value);
    }

    /**
     * @return bool
     */
    public function getToggle(): bool
    {
        return $this->getAttribute('is_active');
    }
}
