<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Security;

use Illuminate\Auth\EloquentUserProvider;
use Inisiatif\Component\Auth\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
final class InisiatifUserProvider extends EloquentUserProvider
{
    /**
     * @param mixed $identifier
     *
     * @return Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        /** @var User|null $user */
        $user = parent::retrieveById($identifier);

        if ($user === null) {
            return $user;
        }

        return $user->getToggle() ? $user : null;
    }

    /**
     * @param mixed  $identifier
     * @param string $token
     *
     * @return Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        /** @var User|null $user */
        $user = parent::retrieveByToken($identifier, $token);

        if ($user === null) {
            return $user;
        }

        return $user->getToggle() ? $user : null;
    }

    /**
     * @param array $credentials
     *
     * @return Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        /** @var User|null $user */
        $user = parent::retrieveByCredentials($credentials);

        if ($user === null) {
            return $user;
        }

        return $user->getToggle() ? $user : null;
    }
}
