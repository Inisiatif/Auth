<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Repositories;

use Inisiatif\Component\Contract\User\Model\UserInterface;
use Inisiatif\Component\Resource\Laravel\Eloquent\Repository;
use Inisiatif\Component\Contract\Resource\Model\ResourceInterface;
use Inisiatif\Component\Contract\User\Repository\UserRepositoryInterface;
use Inisiatif\Component\Contract\Resource\Repository\HasLegacyIntranetRepositoryInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
final class UserRepository extends Repository implements UserRepositoryInterface, HasLegacyIntranetRepositoryInterface
{
    /**
     * @param UserInterface $resource
     */
    public function __construct(UserInterface $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @param string|integer|null $id
     *
     * @return UserInterface|null
     */
    public function findById($id): ?UserInterface
    {
        /** @var UserInterface|null $user */
        $user = $this->model->newQuery()->find($id);

        return $user;
    }

    /**
     * @param string $email
     *
     * @return UserInterface|null
     */
    public function findOneByEmail(string $email): ?UserInterface
    {
        /** @var UserInterface|null $user */
        $user = $this->model->newQuery()->where('email', $email)->first();

        return $user;
    }

    /**
     * @param string|integer $intranetId
     *
     * @return ResourceInterface|null
     */
    public function findOneByIntranetId($intranetId): ?ResourceInterface
    {
        /** @var UserInterface|null $user */
        $user = $this->model->newQuery()->where('intranet_id', $intranetId)->first();

        return $user;
    }
}
