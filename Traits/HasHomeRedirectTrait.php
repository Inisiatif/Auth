<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Traits;

use Illuminate\Routing\Redirector;
use Illuminate\Http\RedirectResponse;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
trait HasHomeRedirectTrait
{
    use HasRedirectTo;

    /**
     * @param Redirector $redirect
     *
     * @return RedirectResponse
     */
    public function home(Redirector $redirect): RedirectResponse
    {
        return $redirect->to(
            $this->redirectTo()
        );
    }
}
