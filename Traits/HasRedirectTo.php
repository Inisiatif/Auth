<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Traits;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
trait HasRedirectTo
{
    /**
     * @return string
     */
    abstract public function redirectTo(): string;
}
