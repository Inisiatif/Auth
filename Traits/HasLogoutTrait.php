<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Traits;

use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;
use Illuminate\Auth\AuthManager;
use Illuminate\Auth\SessionGuard;
use Illuminate\Routing\Redirector;
use Illuminate\Http\RedirectResponse;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
trait HasLogoutTrait
{
    use HasRedirectTo;

    /**
     * @param AuthManager $auth
     * @param JWT         $jwt
     *
     * @return RedirectResponse|Redirector
     */
    public function logout(AuthManager $auth, JWT $jwt)
    {
        /** @var SessionGuard $web */
        $web = $auth->guard('web');

        try {
            $token = $jwt->getToken() ?: $web->getSession()->get('jwt');

            if ($token !== null) {
                /** @var JWTGuard $api */
                $api = $auth->guard('api');

                $api->setToken($token);

                $api->logout(true);
            }
        } catch (\Throwable $exception) {
        }

        $web->getSession()->flush();
        $web->logout();

        return \redirect(
            $this->redirectTo()
        );
    }

    /**
     * @return string
     */
    public function redirectTo(): string
    {
        return 'https://accounts.inisiatif.id/logout';
    }
}
