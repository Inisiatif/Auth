<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Traits;

use Illuminate\Support\Str;
use Laravel\Socialite\Contracts\Factory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Inisiatif\Component\Socialite\Providers\InisiatifProvider;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
trait HasSocialiteLoginTrait
{
    /**
     * @param Factory $manager
     *
     * @return RedirectResponse
     */
    public function login(Factory $manager): RedirectResponse
    {
        $identifier = Str::lower(InisiatifProvider::IDENTIFIER);

        /** @var InisiatifProvider $driver */
        $driver = $manager->driver($identifier);

        return $driver->stateless()->redirect();
    }
}
