<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Traits;

use RuntimeException;
use Laravel\Socialite\Two;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Tymon\JWTAuth\JWTGuard;
use Illuminate\Auth\AuthManager;
use Illuminate\Auth\SessionGuard;
use Illuminate\Http\RedirectResponse;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Laravel\Socialite\Contracts\Factory;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Auth\Authenticatable;
use Inisiatif\Component\Auth\Factories\UserFactory;
use Inisiatif\Component\Contract\User\Model\UserInterface;
use Inisiatif\Component\Socialite\Providers\InisiatifProvider;
use Inisiatif\Component\Contract\User\Service\UserServiceInterface;
use Inisiatif\Component\Contract\Resource\Repository\HasLegacyIntranetRepositoryInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
trait HasSocialiteCallbackTrait
{
    use HasRedirectTo;

    /**
     * @var string
     */
    protected $token = null;

    /**
     * @var AuthManager
     */
    protected $auth;

    /**
     * @var Repository
     */
    protected $config;

    /**
     * @var HasLegacyIntranetRepositoryInterface
     */
    protected $repository;

    /**
     * @param AuthManager                          $auth
     * @param Repository                           $config
     * @param HasLegacyIntranetRepositoryInterface $repository
     */
    public function __construct(AuthManager $auth, Repository $config, HasLegacyIntranetRepositoryInterface $repository)
    {
        $this->auth = $auth;

        $this->config = $config;

        $this->repository = $repository;
    }

    /**
     * @param Factory              $manager
     * @param UserServiceInterface $service
     *
     * @return RedirectResponse
     */
    public function callback(Factory $manager, UserServiceInterface $service)
    {
        $identifier = Str::lower(InisiatifProvider::IDENTIFIER);

        /** @var InisiatifProvider $driver */
        $driver = $manager->driver($identifier);

        /** @var Two\User $oauthUser */
        $oauthUser = $driver->stateless()->user();

        if (! $this->canAccessApplication($oauthUser)) {
            $to = $this->redirectError(403);

            return \redirect($to);
        }

        /** @var UserInterface|null $user */
        $user = $this->repository->findOneByIntranetId($oauthUser->getId());

        if ($user === null) {
            $model = UserFactory::buildFromOAuth($oauthUser);

            $user = $service->save($model);
        }

        return $this->responseLogin($user);
    }

    /**
     * @param int $code
     *
     * @return string
     */
    abstract public function redirectError(int $code = 403): string;

    /**
     * @param Two\User $user
     *
     * @return bool
     */
    protected function canAccessApplication($user): bool
    {
        if ($this->config->get('services.inisiatif.access_check') === false) {
            return true;
        }

        $applications = Arr::get($user->getRaw(), 'applications');

        return (boolean) Arr::where($applications, function ($application) {
            return (int) $application['id'] === (int) config('services.inisiatif.app_id');
        });
    }

    /**
     * @param UserInterface $user
     *
     * @return RedirectResponse
     */
    protected function responseLogin(UserInterface $user)
    {
        if (! $user instanceof JWTSubject) {
            throw new RuntimeException(\sprintf('User class `%s` must be instance of `%s`', \get_class($user), JWTSubject::class));
        }

        /** @var JWTGuard $jwt */
        $jwt = $this->auth->guard('api');

        $this->token = $jwt->login($user);

        if ($user instanceof Authenticatable) {
            /** @var SessionGuard $web */
            $web = $this->auth->guard('web');

            $web->login($user);

            $web->getSession()->put('jwt', $this->token);
        }

        return \redirect(
            $this->redirectTo()
        );
    }
}
