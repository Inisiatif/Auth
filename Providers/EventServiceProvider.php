<?php declare(strict_types=1);

namespace Inisiatif\Component\Auth\Providers;

use SocialiteProviders\Manager\SocialiteWasCalled;
use Inisiatif\Component\Socialite\Listeners\InisiatifExtendSocialite;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
final class EventServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $listen = [
        SocialiteWasCalled::class => [
            InisiatifExtendSocialite::class,
        ],
    ];
}
